package classes;

import interfaces.Financiavel;

public class Apartamento extends ItemDeMostruario implements Financiavel{
    protected int NumberOfFloors;
    protected int NumberOfRooms;
    protected double ConstructedArea;
    protected String Location;
    protected boolean Availability;

    public void vender(){
        this.Availability = false;
    }
    public void alugar(){
        this.Availability = false;
    }
    public void reformar(){
        this.Availability = false;
    }


    public Apartamento(int NumberOfFloors, int NumberOfRooms, double ConstructedArea, String Location, boolean Availability){
        super();
        this.setNumberOfFloors(NumberOfFloors);
        this.setNumberOfRooms(NumberOfRooms);
        this.setConstructedArea(ConstructedArea);
        this.setLocation(Location);
        this.setAvailability(Availability);
    }

    public void setNumberOfFloors(int NumberOfFloors){
        this.NumberOfFloors=NumberOfFloors;
    }
    public int getNumberOfFloors(){
        return this.NumberOfFloors;
    }


    public void setNumberOfRooms(int NumberOfRooms){
        this.NumberOfRooms=NumberOfRooms;
    }
    public int getNumberOfRooms(){
        return this.NumberOfRooms;
    }


    public void setConstructedArea(double ConstructedArea){
        this.ConstructedArea=ConstructedArea;
    }
    public double getConstructedArea(){
        return this.ConstructedArea;
    }


    public void setLocation(String Location){
        this.Location=Location;
    }
    public String getLocation(){
        return this.Location;
    }


    public void setAvailability(boolean Availability){
        this.Availability=Availability;
    }
    public boolean getAvailability(){
        return this.Availability;
    }

    @Override
    public void financiar(){
        System.out.println("Apartamento financiado.");
        this.Availability = false;
    }

    @Override
    public void AumentarDesconto(){
        this.Desconto = Desconto+10;
    }
}




