package classes;

public class ItemDeMostruario {
    protected double Price;
    protected String Name;
    protected boolean InStock;
    protected double Desconto;
    protected String DataDeUpload;
    protected String DataDeVenda;

    public ItemDeMostruario(double Price, String Name, boolean Instock, double Desconto, String DataDeUpload, String DataDeVenda){
        this.setPrice(Price);
        this.setName(Name);
        this.setInStock(InStock);
        this.setDesconto(Desconto);
        this.setDataDeUpload(DataDeUpload);
        this.setDataDeVenda(DataDeVenda);
    }
    public ItemDeMostruario(){
        this.setName(Name = "Cobertura");
        this.setPrice(Price = 1.20);
        this.setInStock(InStock = false);
        this.setDesconto(Desconto = 0.20);
        this.setDataDeUpload(DataDeUpload = "20/07/2022");
        this.setDataDeVenda(DataDeVenda="20/10/2022");
    }

    public void setPrice(double Price){
        this.Price=Price;
    }
    public double getPrice(){
        return this.Price;
    }

    public void setName(String Name){
        this.Name=Name;
    }
    public String getName(){
        return this.Name;
    }

    public void setInStock(boolean InStock){
        this.InStock=InStock;
    }
    public boolean getInStock(){
        return this.InStock;
    }

    public void setDesconto(double Desconto){
        this.Desconto =Desconto;
    }
    public double getDesconto(){
        return this.Desconto;
    }

    public void setDataDeUpload(String DataDeUpload){
        this.DataDeUpload=DataDeUpload;
    }
    public String getDataDeUpaload(){
        return this.DataDeUpload;
    }
    public void setDataDeVenda(String DataDeVenda){
        this.DataDeVenda=DataDeVenda;
    }
    public String getDataDeVenda(){
        return this.DataDeVenda;
    }

    public void AumentarDesconto(){
        this.Desconto = Desconto+5;
    }
}
