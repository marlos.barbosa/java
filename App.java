import classes.Apartamento;

public class App {
    public static void main(String[] args){
        int NumberOfFloors = 5;
        int NumberOfRooms = 6;
        double ConstructedArea = 25.0;
        String Location = "SP";
        boolean Availability = true;

        Apartamento apartamento1 = new Apartamento(NumberOfFloors,NumberOfRooms,ConstructedArea,Location,Availability);
        System.out.println(apartamento1.getAvailability());
        apartamento1.financiar();
        System.out.println(apartamento1.getAvailability());
        System.out.println(apartamento1.getName());
        System.out.println(apartamento1.getDesconto());
        apartamento1.AumentarDesconto();
        System.out.println(apartamento1.getDesconto());
    }
}
